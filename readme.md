# Dell Inspiron 3458 - macOS Mojave

## Basic Specification
|Status (Mojave)|Item|Spec|
|---|---|---|
|√|Prosesor|intel Core i5200 U Broadwell |
|√|RAM|8GB DDR3 1600Mhz|
|√|Disk|SSD WD Blue 250 GB |
|√|Graphic|Intel HD 5500 |
|-|Graphic Switchable| NVIDIA GeForce GT820 (Disabled)|
|- / √|Audio Code | ALC3234 with VoodooHDA |
|-|Wi-Fi | AR9565 |
|√|Ethernet | RTL8101E |
|-|Bluetooth | |
|-|Camera| |
|-|Card Reader| |


## Tentang Hackinstosh Mojave

### Tantangan Utama
- APFS not HFS +
- Kext Berubah
- Inject tidak jalan lagi

### Hal Baru
- Lilu
- Whatever Green

## Here We Go !!
### Alat Tempur
- USB Flashdisk sebaiknya lebih dari 1
	- Satu untuk installer Mac/OS X
	- Satu Live USB untuk Linux, in case *karnel panic*

### General Prep
- Sabar
- Jangan takut gagal
- Mau belajar, walaupun sedikit demi sedikit
- Punya semangat menyelidiki

### Perisapan BIOS Settings
- Setting BIOS
	- Matikan Swicthable Graphic
	- Disable semua tentang V/T dan V/Td
	- Set Boot ke EUFI
	- Legacy OFF

### Membuat Installer Mojave
> Note : Jika Anda memiliki akses ke mesin dengan OS Machintosh (10.10 atau diatasnya) langsung ke bagian membuat bootable Mojave melalui Mac. Jika belum lakukan alternatif install El Capitan atau VirtualBox yang akan di bahas pada dokumen kali ini.

#### Install El Capitan
- Membuat flash disk Bootable di Windows 
	- Download Transmac melalui [website resmi](https://www.acutesystems.com/scrtm.htm)
	- Download OSX El Capitan + MBR Patch + Clover (link disini)
	- Lakukan hal dibawah ini melalui TransMac
		1. Pertama, Format USB
		2. Kedua, Restore Image
	- Selesai, Flash disk Anda telah terdapat Installer El Capitan
- Langkah Instalasi, secara umum dibagi menjadi dua bagian, bagian sampai Installer dan pasca installer
	- Sampai Installer
		1. Pastikan BIOS booting dari EFI Flashdisk Anda
		2. Masuk ke CLOVER, pilih Install OSX From <Nama Flashdisk>. Tidak perlu mengganti bootflag atau yang lainnya. Pastikan Masuk dengan mode verbose, sehingga tahu jika terdapat masalah.
		3. Lakukan instalasi sesuai dengan perintah di Layar. Termasuk melalukan partisi Harddisk.
		4. Setelah installer selasai, komputer akan di restart. Lanjut ke bagian kedua
	- Post Instalasi
		1. Masuk lagi ke CLOVER, booting dari "Boot OSX from <nama harddisk>"
		2. Masuk ke Mac, pastikan sampai Desktop
		3. Mount disk EFI. Lakukan dengan perintah terminal `mkdir EFI` dan `sudo mount -t msdos /dev/diskXsY EFI/`, atau dengan [Clover Configuration](https://mackie100projects.altervista.org/download-clover-configurator/), mount EFI
		4. Selanjutnya, download Clover di [sini](https://sourceforge.net/projects/cloverefiboot/), install Clover di Volume/Partisi/Hasil Mount `EFI`
		5. Alternatif nomor 4, Anda Bisa Langsung copy seluruh isi folder EFI di flash disk ke partisi EFI.
		6. Jika berhasil, restart
		7. Langkah selanjutnya ikuti panduan di [sini](https://bitbucket.org/condro/hackintosh-el-capitan-3458/src/master/)

#### Buat Bootable untuk Mojave
Main source :  [Youtube - 
HACKINTOSH GUIDE How to Install MacOS Mojave !!!](https://www.youtube.com/watch?v=VdRSYogDygs)

> Note : Mojave secara default menggunakan APFS sebagai file sistem, CLOVER belum stabil dalam implementasi driver APFS, sehingga perlu penyesuaian di Installernya supaya bisa di Install di HFS seperti OS X sebelumnya.

1. Download Mac OS Mojave from App Store
2. Download Mojave MBR Patch: [https://goo.gl/5bRvYw](https://goo.gl/5bRvYw)
3. Download Clover EFI Bootloader: [https://goo.gl/qHvzqk](https://goo.gl/qHvzqk)
4. Download Clover Configurator: [https://goo.gl/kK7zwX](https://goo.gl/kK7zwX)
5. Insert USB Thumb Drive and format it.
6. Extract and Run Mojave MBR Patch as on the video to create the USB Drive
7. Extract Clover Configurator, run and Mount EFI USB drive, Open it (its empty at this moment)
◉ Optional: At this moment you may place my EFI folder in your USB Drive EFI partition and start the installation.
8. Extract and run Clover EFI Bootloader to create EFI Folder
9. Download these Kexts and place them in EFI/CLOVER/Kexts/Other
	1. (Required) FakeSMC: [https://goo.gl/ruLx4y] (https://goo.gl/ruLx4y)
	2. (Optional) Power management: https://goo.gl/mzhU2v
	3. (Optional) USB Inject: https://goo.gl/kUjr39
	4. (Optional) AppleALC: https://goo.gl/hd9Auz
	5. (Later Required At this moment Optional) Lilu: https://goo.gl/uVTUPB
	6. (Later Required At this moment Optional) Ethernet: https://goo.gl/XY3FJq 
	7. (Later Required At this moment Optional) Whatever Green: https://goo.gl/TuQnUJ
	8. PS2

10. Reboot the computer and boot from USB Flash Drive
11. On the Clover Menu select - USB Drive instalation and follow the video
12. Once it finishes it will reboot
13. On boot select USB Flash Drive, but now on Clover Menu Screen, select the SSD that we started the installation
14. Once it finishes we have a working Hackintosh
15. Use Clover Configurator to Mount and Open EFI Partitions of USB Flash Drive and SSD. Copy the EFI Folder from USB to the SSD.
16. Rebbot the system and Make the SSD the permanent boot disk 
17. Congratulations, we have a working and stable hackintosh 

### POST INSTALL
- Install Clover di HDD, kemudian kopi saja semua file EFI di repo ini, termasuk Lilu, Whatevergreen, Config.plist. This will solve the display issue and give 2048 video memory as HD5500

- Kexts
	- Install Tool ( Clover Config, DSTI, Kext Utility) 
	- Ethernet, working from prev version Realtek 8100 (Vietnam Tool)  
	- Sound, VoodooHDA from [here](https://github.com/chris1111/VoodooHDA-2.9.0-Clover-V12)
	- Wi-Fi, did not work. I use TP Link TL-WN725N, with driver from its official website

### Congratulation, from this point you should use the Mojave in proper way

## Credit
- Everyone named in every credit on all link above
- Hackintosh Indonesia FB group